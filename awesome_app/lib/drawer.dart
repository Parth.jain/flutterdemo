import 'package:flutter/material.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          /* DrawerHeader(
              child: Text(
                "Hi, Drawer is Here",
                style: TextStyle(color: Colors.white),
              ),
              decoration: BoxDecoration(color: Colors.purple)), */
          UserAccountsDrawerHeader(
              accountName: Text("Parth jain"),
              accountEmail: Text("parthjainh@gmail.com"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(
                    "https://pbs.twimg.com/profile_images/1183295722780688385/kU2AZJR1_400x400.jpg"),
              )),
          /* Image.network("https://pbs.twimg.com/profile_images/1183295722780688385/kU2AZJR1_400x400.jpg")*/
          ListTile(
            leading: Icon(Icons.person),
            title: Text("Account"),
            subtitle: Text("Personal"),
            trailing: Icon(Icons.edit),
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text("Email"),
            subtitle: Text("parthjainh@gmail.com"),
            trailing: Icon(Icons.send),
          )
        ],
      )
      );
  }
}
