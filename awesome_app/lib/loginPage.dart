import 'package:awesome_app/bgImage.dart';
import 'package:awesome_app/homePage.dart';
import 'package:awesome_app/utils/PrefConstants.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final userNameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Login Page")),
      body: Stack(
        fit: StackFit.expand,
        children: [
          BgImage(),
          Center(
            child: SingleChildScrollView(
              child: Card(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Form(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                decoration: InputDecoration(
                                    hintText: "Enter User Name",
                                    labelText: "User Name"),
                              ),
                              SizedBox(height: 20),
                              TextFormField(
                                obscureText: true,
                                decoration: InputDecoration(
                                    hintText: "Enter Password",
                                    labelText: "Password"),
                              ),
                              SizedBox(height: 20),
                              RaisedButton(
                                onPressed: () {
                                  PrefConstants.prefs.setBool("isLoggedIn", true);

                                  //Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage())); 
                                    Navigator.pushReplacementNamed(context, "/home");  //Need to pass arguments if use route else using constructor as shown in above line
                                },
                                child: Text("Sign in"),
                                color: Colors.orange,
                                textColor: Colors.white,
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
