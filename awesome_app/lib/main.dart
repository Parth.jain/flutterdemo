import 'package:awesome_app/homePage.dart';
import 'package:awesome_app/loginPage.dart';
import 'package:awesome_app/utils/PrefConstants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';


Future main() async{
WidgetsFlutterBinding.ensureInitialized();
PrefConstants.prefs =await SharedPreferences.getInstance();

  runApp(
      MaterialApp(
        title: "Awesome Flutter App",
        home: PrefConstants.prefs.getBool("isLoggedIn")==true?HomePage():LoginPage(),
        theme: ThemeData(primarySwatch: Colors.purple),
         routes: {
        "/login": (context) => LoginPage(),
        "/home": (context) => HomePage()
      }
      ),
     );

}
