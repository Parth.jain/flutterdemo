import 'package:bloc_shopping_cart_flutter/bloc/cart_items_bloc.dart';
import 'package:flutter/material.dart';

class Checkout extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Checkout')),
      body: StreamBuilder(
        stream: bloc.getStream,
        initialData: bloc.allItems,
        builder: (context, snapshot) {
          return snapshot.data['cart items'].length > 0
              ? Column(
            children: <Widget>[
              Expanded(child: checkoutListBuilder(snapshot)),
              RaisedButton(
                onPressed: () {
                  var shopList = snapshot.data["cart items"];
                  var total=0;
                  for(var i=0;i<snapshot.data['cart items'].length;i++ ){
                    total=total+shopList[i]['price'];
                  }
                  //TODo Checkout items function needs to create
                  showAlertDialog(context,total);
                },
                child: Text("Checkout"),
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(height: 40)
            ],
          )
              : Center(child: Text("You haven't taken any item yet"));
        },
      ),
    );
  }
}

showAlertDialog(BuildContext context,total) {

  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () { },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("My title"),
    content: Text("Your checkout Total is: \$${total}"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

Widget checkoutListBuilder(snapshot) {
  return ListView.builder(
    itemCount: snapshot.data["cart items"].length,
    itemBuilder: (BuildContext context, i) {
      final cartList = snapshot.data["cart items"];
      return ListTile(
        title: Text(cartList[i]['name']),
        subtitle: Text("\$${cartList[i]['price']}"),
        trailing: IconButton(
          icon: Icon(Icons.remove_shopping_cart),
          onPressed: () {
            bloc.removeFromCart(cartList[i]);
          },
        ),
        onTap: () {},
      );
    },
  );
}